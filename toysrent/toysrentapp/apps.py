from django.apps import AppConfig


class ToysrentappConfig(AppConfig):
    name = 'toysrentapp'
